from rest_framework import viewsets, generics
from jobs.serializers import (
    JobSerializer, CompanySerializer, JobResponseSerializer)
from jobs.models import Job, JobComment, JobResponse, Company, CompanyComment


class JobViewSet(viewsets.ModelViewSet):
    """
    Jobs
    """
    queryset = Job.objects.all()
    serializer_class = JobSerializer


class CompanyViewSet(viewsets.ModelViewSet):
    """
    Companies
    """
    queryset = Company.objects.all()
    serializer_class = CompanySerializer


class JobResponseViewSet(viewsets.ModelViewSet):
    serializer_class = JobResponseSerializer
    def get_queryset(self):
        print self.kwargs
        job_id = self.kwargs['job_id']
        return JobResponse.objects.filter(job__id=job_id)


class JobResponseList(generics.ListAPIView):
    serializer_class = JobResponseSerializer

    def get_queryset(self):
        """
        This view should return a list of all the purchases for
        the user as determined by the username portion of the URL.
        """
        job_id = self.kwargs['job_id']
        return JobResponse.objects.filter(job__id=job_id)