# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2017-01-18 15:12
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name=b'Created At')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name=b'Updated At')),
                ('title', models.CharField(max_length=150, verbose_name=b'Title')),
                ('description', models.TextField(verbose_name=b'Description')),
                ('site', models.CharField(max_length=150, verbose_name=b'Site url')),
            ],
            options={
                'db_table': 'companies',
            },
        ),
        migrations.CreateModel(
            name='CompanyComment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name=b'Created At')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name=b'Updated At')),
                ('title', models.CharField(max_length=150, verbose_name=b'Title')),
                ('description', models.TextField(verbose_name=b'Description')),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='jobs.Company', verbose_name=b'Company')),
            ],
            options={
                'db_table': 'company_comments',
            },
        ),
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name=b'Created At')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name=b'Updated At')),
                ('title', models.CharField(max_length=150, verbose_name=b'Title')),
                ('description', models.TextField(verbose_name=b'Description')),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='jobs.Company', verbose_name=b'Company')),
            ],
            options={
                'db_table': 'jobs',
            },
        ),
        migrations.CreateModel(
            name='JobComment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name=b'Created At')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name=b'Updated At')),
                ('title', models.CharField(max_length=150, verbose_name=b'Title')),
                ('description', models.TextField(verbose_name=b'Description')),
                ('job', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='jobs.Job', verbose_name=b'Job')),
            ],
            options={
                'db_table': 'job_comments',
            },
        ),
        migrations.CreateModel(
            name='JobResponse',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name=b'Created At')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name=b'Updated At')),
                ('title', models.CharField(max_length=150, verbose_name=b'Title')),
                ('description', models.TextField(verbose_name=b'Description')),
                ('my_rank', models.PositiveSmallIntegerField(choices=[(1, b'1'), (2, b'2'), (3, b'3'), (4, b'4'), (5, b'5')], verbose_name=b'My Rank')),
                ('is_positive', models.BooleanField(verbose_name=b'Is Positive?')),
                ('job', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='jobs.Job', verbose_name=b'Job')),
            ],
            options={
                'db_table': 'job_responses',
            },
        ),
    ]
