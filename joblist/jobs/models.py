from django.db import models


class AbstractBaseModel(models.Model):
    
    created_at = models.DateTimeField("Created At", auto_now_add=True)
    updated_at = models.DateTimeField("Updated At", auto_now=True)
    title = models.CharField("Title", max_length=150)
    description = models.TextField("Description")
    
    class Meta:
        abstract = True


class AbstractBaseComment(AbstractBaseModel):
    
    class Meta:
        abstract = True


class Company(AbstractBaseModel):
    
    site = models.CharField("Site url", max_length=150)

    class Meta:
        db_table = 'companies'


class CompanyComment(AbstractBaseModel):
    
    company = models.ForeignKey(Company, verbose_name="Company")
    
    class Meta:
        db_table = 'company_comments'


class Job(AbstractBaseModel):
    
    company = models.ForeignKey(Company, verbose_name="Company", db_column='company_id')
    
    class Meta:
        db_table = 'jobs'


class JobComment(AbstractBaseModel):
    
    job = models.ForeignKey(Job, verbose_name="Job")
    
    class Meta:
        db_table = 'job_comments'


class JobResponse(AbstractBaseModel):
    
    RANK_CHOICES = [(x, str(x)) for x in range(1, 6)]
    
    job = models.ForeignKey(Job, verbose_name="Job", related_name='responses')
    my_rank = models.PositiveSmallIntegerField("My Rank", choices=RANK_CHOICES)
    is_positive = models.BooleanField("Is Positive?")
    class Meta:
        db_table = 'job_responses'
