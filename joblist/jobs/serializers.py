from jobs.models import Job, Company, CompanyComment, JobComment, JobResponse
from rest_framework import serializers


class JobSerializer(serializers.HyperlinkedModelSerializer):
    company_title = serializers.CharField(
        source="company.title",
        read_only=True)
    company = serializers.IntegerField(source="company_id")
    class Meta:
        model = Job
        fields = ('url', 'id', 'title', 'description', 'company', 'company_title')


class JobCommentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = JobComment
        fields = ('url', 'id', 'title', 'job__title', 'job__company__title', 'job_id')

class JobResponseSerializer(serializers.HyperlinkedModelSerializer):
    job_id = serializers.IntegerField()
    lookup_field = 'id'
    class Meta:
        model = JobResponse
        fields = ('url', 'id', 'title', 'description', 'my_rank', 'is_positive', 'job_id')


# class JobWithResponsesSerializer(serializers.HyperlinkedModelSerializer):
#     responses = JobResponseSerializer(many=True, read_only=True)
#     company = serializers.IntegerField(source="company_id")
#     class Meta:
#         model = Job
#         fields = ('url', 'id', 'title', 'description', 'company', 'responses')


class CompanySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Company
        fields = ('url', 'id', 'title', 'description')


class CompanyCommentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CompanyComment
        fields = ('url', 'id', 'title', 'company__title', 'company_id')
