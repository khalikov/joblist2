import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Company } from '../components/job/company';

@Injectable()
export class CompanyService {
    private apiURL = 'http://localhost:8000/companies/';

    constructor(public http: Http) { }

    getCompanies() {
        let headers = new Headers({
            'Content-Type': 'application/json'
        });
        let options = new RequestOptions({ headers: headers });
        return this.http.get(this.apiURL + '?format=json', options)
              .toPromise()
              .then(response => response.json())
              .catch(this.handleError);
    }
    getCompany(id: number){
        return this.http.get(this.apiURL + id + '?format=json')
                .toPromise()
                .then(response => response.json())
                .catch(this.handleError);
    }

    save(company: Company): Promise<Company> {
        if (company.id) {
            return this.put(company);
        }
        return this.post(company);
    }
    // Add new Company
    private post(company: Company): Promise<Company> {
        let headers = new Headers({
            'Content-Type': 'application/json'
        });

        return this.http
                  .post(this.apiURL, JSON.stringify(company), { headers: headers })
                  .toPromise()
                  .then(res => res.json())
                  //.catch(this.handleError);
    }

    // Update existing Company
    private put(company: Company): Promise<Company> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiURL}${company.id}/`;

        return this.http
                  .put(url, JSON.stringify(company), { headers: headers })
                  .toPromise()
                  .then(() => company)
                  //.catch(this.handleError);
    }
    delete(company: Company): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiURL}${company.id}/`;

        return this.http
            .delete(url, { headers: headers })
            .toPromise()
            .catch(this.handleError);
    }
    private handleError(error: any) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}