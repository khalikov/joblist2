import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Job } from '../components/job/job';
import { JobResponse } from '../components/job/job-response';

@Injectable()
export class JobService {
    private host = 'http://localhost:8000/';
    private apiURL = this.host + 'jobs/';
    private getJobApiUrl = this.host + 'jobs-and-responses/';
    private responseApiURL = this.host + 'responses/';

    constructor(public http: Http) { }

    getJobs() {
        let headers = new Headers({
            'Content-Type': 'application/json'
        });
        let options = new RequestOptions({ headers: headers });
        return this.http.get(this.apiURL + '?format=json', options)
              .toPromise()
              .then(response => response.json())
              .catch(this.handleError);
    }
    getJob(id: number){
        return this.http.get(this.apiURL + id + '?format=json')
                .toPromise()
                .then(response => response.json())
                .catch(this.handleError);
    }



    save(job: Job): Promise<Job> {
        if (job.id) {
            return this.put(job);
        }
        return this.post(job);
    }
    // Add new Job
    private post(job: Job): Promise<Job> {
        let headers = new Headers({
            'Content-Type': 'application/json'
        });

        return this.http
                  .post(this.apiURL, JSON.stringify(job), { headers: headers })
                  .toPromise()
                  .then(res => res.json())
                  //.catch(this.handleError);
    }

    // Update existing Job
    private put(job: Job): Promise<Job> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiURL}${job.id}/`;

        return this.http
                  .put(url, JSON.stringify(job), { headers: headers })
                  .toPromise()
                  .then(() => job)
                  //.catch(this.handleError);
    }
    delete(job: Job): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiURL}${job.id}/`;

        return this.http
            .delete(url, { headers: headers })
            .toPromise()
            .catch(this.handleError);
    }
    getResponses(job) {
        let headers = new Headers({
            'Content-Type': 'application/json'
        });
        let options = new RequestOptions({ headers: headers });
        let url = `${this.apiURL}${job.id}/responses/?format=json`;
        return this.http.get(url, options)
              .toPromise()
              .then(response => response.json())
              .catch(this.handleError);
    }
    saveResponse(response: JobResponse): Promise<JobResponse> {
        if (response.id) {
            return this.putResponse(response);
        }
        return this.postResponse(response);
    }
    // Add new Job
    private postResponse(response: JobResponse): Promise<JobResponse> {
        let headers = new Headers({
            'Content-Type': 'application/json'
        });

        return this.http
                  .post(this.responseApiURL, JSON.stringify(response), { headers: headers })
                  .toPromise()
                  .then(res => res.json())
                  //.catch(this.handleError);
    }

    // Update existing Job
    private putResponse(response: JobResponse): Promise<JobResponse> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.responseApiURL}${response.id}/`;

        return this.http
                  .put(url, JSON.stringify(response), { headers: headers })
                  .toPromise()
                  .then(() => response)
                  //.catch(this.handleError);
    }
    deleteResponse(response: JobResponse): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiURL}${response.id}/`;

        return this.http
            .delete(url, { headers: headers })
            .toPromise()
            .catch(this.handleError);
    }
    private handleError(error: any) {
        console.error('An error occurred', error);
        return Promise.reject(error);
    }
}