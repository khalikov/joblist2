import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { JobComponent } from './components/job/job.component';
import { JobDetailComponent } from './components/job/job-detail.component';
import { JobResponseDetailComponent } from './components/job/job-response-detail.component';
import { routedComponents, AppRoutingModule } from './app-routing.module';
import { JobService } from './services/job.service';

@NgModule({
    imports: [NgbModule.forRoot(), BrowserModule, FormsModule, HttpModule, RouterModule, AppRoutingModule],
    declarations: [AppComponent, routedComponents, JobResponseDetailComponent],
    bootstrap: [AppComponent],
    providers: [{provide: APP_BASE_HREF, useValue: '/'}, JobService]
})
export class AppModule {}