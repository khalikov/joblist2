import { Component } from '@angular/core';
import { JobComponent } from './components/job/job.component';
import { JobService } from './services/job.service';
import { CompanyService } from './services/company.service'
@Component({
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: 'app.component.html',
    providers: [JobService, CompanyService]
})

export class AppComponent {
    title:string = 'Job List'
    subtitle: string = 'Personal list of job applications and responses'
}
