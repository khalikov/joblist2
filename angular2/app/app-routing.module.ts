import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JobComponent } from './components/job/job.component';
import { JobDetailComponent } from './components/job/job-detail.component';
import { CompanyComponent } from './components/job/company.component';
import { CompanyDetailComponent } from './components/job/company-detail.component';
// import { HeroesComponent } from './heroes.component';
// import { HeroDetailComponent } from './hero-detail.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/jobs',
    pathMatch: 'full'
  },
  {
    path: 'jobs',
    component: JobComponent
  },
  {
     path: 'job/detail/:id',
     component: JobDetailComponent
  },
  {
    path: 'companies',
    component: CompanyComponent
  },
  {
     path: 'company/detail/:id',
     component: CompanyDetailComponent
  },
  // {
  //   path: 'heroes',
  //   component: HeroesComponent
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routedComponents = [
  JobComponent, JobDetailComponent, CompanyComponent, CompanyDetailComponent
]
//[DashboardComponent, HeroesComponent, HeroDetailComponent];