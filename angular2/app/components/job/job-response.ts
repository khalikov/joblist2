export class JobResponse {
    id: number;
    title: string;
    description: string;
    my_rank: number = 3;
    is_positive: boolean = false;
    job_id: number;
}