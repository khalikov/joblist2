import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Job } from './job';
import { JobResponse } from './job-response';
import { JobService } from '../../services/job.service';
import { CompanyService } from '../../services/company.service';

import { JobDetailComponent } from './job-detail.component';

@Component({
    moduleId: module.id,
    selector: 'my-response-detail',
    templateUrl: 'job-response-detail.component.html'
})
export class JobResponseDetailComponent implements OnInit {
    ranks: any = [
        {
            'id': 1, 'title': '1',
        },
        {
            'id': 2, 'title': '2',
        },
        {
            'id': 3, 'title': '3',
        },
        {
            'id': 4, 'title': '4',
        },
        {
            'id': 5, 'title': '5',
        }
    ]
    is_positive_choices = [
        {
            'id': true, 'title': 'Yes'
        },
        {
            'id': false, 'title': 'No'
        },
           
    ]
    @Input() job: Job;
    @Input() response: JobResponse;
    @Input() parent: JobDetailComponent;
    @Output() close = new EventEmitter();
    error: any = {};
    navigated = false; // true if navigated here
    
    constructor(
        private jobService: JobService,
        private route: ActivatedRoute,
        private companyService: CompanyService) {
    }

    ngOnInit(): void {
        this.navigated = false;
        this.response = new JobResponse();
        this.response.job_id = this.job.id;
        //this.getCompanies();
    }

    save(): void {
        this.jobService
            .saveResponse(this.response)
            .then(response => {
                this.response = new JobResponse(); // saved job, w/ id if new
                this.response.job_id = this.job.id;
                this.goBack(response);
                this.parent.getResponses();

            })
            .catch(error => {
                console.log(error, 'is jd.c.ts error');
                this.error = error.json();
            }); // TODO: Display error message
    }

    goBack(savedResponse: JobResponse = null): void {
        this.close.emit(savedResponse);
        if (this.navigated) { window.history.back(); }
    }
}