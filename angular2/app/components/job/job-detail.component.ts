import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { Job } from './job';
import { Company } from './company';
import { JobResponse } from './job-response';
import { JobService } from '../../services/job.service';
import { CompanyService } from '../../services/company.service';


@Component({
    moduleId: module.id,
    selector: 'my-job-detail',
    templateUrl: 'job-detail.component.html',
    styleUrls: ['job-detail.component.css']
})
export class JobDetailComponent implements OnInit {
    @Input() job: Job;
    @Output() close = new EventEmitter();
    error: any = {};
    navigated = false; // true if navigated here
    companies: Company[];
    responses: JobResponse[];
    addingResponse: boolean = true;
    selectedResponse: JobResponse;

    constructor(
        private jobService: JobService,
        private route: ActivatedRoute,
        private companyService: CompanyService) {
    }

    ngOnInit(): void {
        this.route.params.forEach((params: Params) => {
            if (params['id'] !== undefined) {
                let id = +params['id'];
                this.navigated = true;
                this.jobService.getJob(id)
                                .then(job => {
                                    this.job = job;
                                    this.getResponses();
                                });
            } else {
                this.navigated = false;
                this.job = new Job();
            }
        });
        this.getCompanies();
    }
    addResponse(): void {
       this.addingResponse = true;
       this.selectedResponse = null;
    }

    save(): void {
        this.jobService
            .save(this.job)
            .then(job => {
                console.log(job);
                this.job = job; // saved job, w/ id if new
                this.goBack(job);
            })
            .catch(error => {
                console.log(error, 'is jd.c.ts error');
                this.error = error.json();
            }); // TODO: Display error message
    }

    goBack(savedJob: Job = null): void {
        this.close.emit(savedJob);
        if (this.navigated) { window.history.back(); }
    }
    getCompanies(): void {
        this.companyService
            .getCompanies()
            .then(companies => this.companies = companies)
            .catch(error => {this.error = error.json()});
    }
    getResponses(): void {
        if (!this.job.id) {
            return;
        }
        this.jobService
            .getResponses(this.job)
            .then(responses => this.responses = responses)
            .catch(error => { this.error = error.json() });
    }
}