import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JobService } from '../../services/job.service'
import { Job } from './job';
import { Company } from './company';

@Component({
    moduleId: module.id,
    selector: 'jobs',
    templateUrl: 'job.component.html',
    styleUrls: ['job.component.css'],
    host: {
        class:'router-outlet-class'
    }
})
export class JobComponent implements OnInit {
    jobs: Job[];
    selectedJob: Job;
    addingJob: boolean = false;
    error: any;

    constructor(public router: Router, public jobService: JobService) { }

    getJobs(): void {
        this.jobService
            .getJobs()
            .then(jobs => this.jobs = jobs)
            .catch(error => this.error = error);
    }

    ngOnInit() {
        this.getJobs();
    }
    onSelect(job: Job): void {
        this.selectedJob = job;
        this.addingJob = false;
    }

    addJob(): void {
       this.addingJob = true;
       this.selectedJob = null;
    }
    gotoDetail(): void {
        this.router.navigate(['job', 'detail', this.selectedJob.id]);
    }
    close(savedJob: Job): void {
        this.addingJob = false;
        if (savedJob) { this.getJobs(); }
    }

    deleteJob(job: Job, event: any): void {
    event.stopPropagation();
    this.jobService
       .delete(job)
       .then(res => {
                this.jobs = this.jobs.filter(h => h !== job);
                if (this.selectedJob === job) { this.selectedJob = null;}
        })
        .catch(error => this.error = error);
    }

}

