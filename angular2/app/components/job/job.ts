import { Company } from './company';
import { JobResponse } from './job-response';
export class Job {
    id: number;
    title: string;
    description: string;
    company: number;
    company_title: string;
    responses: JobResponse[];
}