import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CompanyService } from '../../services/company.service'
import { Company } from './company';

@Component({
    moduleId: module.id,
    selector: 'companies',
    templateUrl: 'company.component.html',
    styleUrls: ['company.component.css']
})
export class CompanyComponent implements OnInit {
    companies: Company[];
    selectedCompany: Company;
    addingCompany: boolean = false;
    error: any;

    constructor(public router: Router, public companyService: CompanyService) { }

    getCompanies(): void {
        this.companyService
            .getCompanies()
            .then(companies => this.companies = companies)
            .catch(error => this.error = error);
    }

    ngOnInit() {
        this.getCompanies();
    }
    onSelect(company: Company): void {
        this.selectedCompany = company;
        this.addingCompany = false;
    }

    addCompany(): void {
       this.addingCompany = true;
       this.selectedCompany = null;
    }
    gotoDetail(): void {
        this.router.navigate(['company', 'detail', this.selectedCompany.id]);
    }
    close(savedCompany: Company): void {
        this.addingCompany = false;
        if (savedCompany) { this.getCompanies(); }
    }

    deleteCompany(company: Company, event: any): void {
    event.stopPropagation();
    this.companyService
       .delete(company)
       .then(res => {
                this.companies = this.companies.filter(h => h !== company);
                if (this.selectedCompany === company) { this.selectedCompany = null;}
        })
        .catch(error => this.error = error);
    }

}

