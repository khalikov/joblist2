# Angular 2 and Django REST Framework

Simple project called JobList is a personal list of applied jobs and their responses

###Set up the Django app:

- Create a virtualenv
- `pip install requirements.txt`
- `cd joblist`
- `python manage.py migrate`
- `python manage.py runserver`

Set up Angular app:

- `cd angular2`
- `npm install`
- `npm start`